// Main Menu

var menuButton = $('#menu-button');
var mainMenu = $('#menu-bg');
var portfolioItem = $('.portfolio-item');

function openMenu(){
    $(menuButton).addClass('menuOpen');
    $(mainMenu).attr('data-showMenu', 'true');
}

function closeMenu(){
    $(menuButton).removeClass('menuOpen');
    $(mainMenu).attr('data-showMenu', 'false');
}

$(menuButton).on('click', function(){
    if ($(mainMenu).attr('data-showMenu') == 'true'){
        closeMenu();
    } else {
        openMenu();
    }
})


$('.menu-link').on('click', function(e){
    e.preventDefault();
    var linkId = $(this).attr('href');
    var pagePosition = $(linkId).offset().top;
    console.log(pagePosition);
    $('html, body').animate({
        scrollTop : pagePosition
    }, 500, closeMenu())
});

$('.portfolio-title button').on('click', function(){
    if ($(this).attr('data-active') == 'false'){
    } else {
    }
})

//Portfolio Content
var portfolioButton = $('.portfolio-item .portfolio-title button');
var closeButton = $('.portfolio-content .close-button');

$(portfolioButton).on('click', function(){
    $(this).parent().parent().find('.portfolio-content').addClass('active');   
})
$(closeButton).on('click', function(){
    $(this).parent().removeClass('active');   
})